const express = require('express');
const bodyParser = require('body-parser');
const app = express();

const { config } = require('./config/index');
const moviesAPi = require('./routes/movies');

moviesAPi(app);

app.get('/', (req, res)=> {
  res.send('Hello World!');
});

app.get('/json', (req, res)=> {
  res.send({json: 'Hey JSON'});
});

app.get('/leap-year/:year', (req, res)=> {
  console.log({year: req.params['year']});
  const year = Number(req.params['year']);
  let isLeapYear = false;

  if(!isNaN(year)){
    isLeapYear = year > 0 && year % 4 === 0;
  }

  res.send({isLeapYear});
});

app.listen(config.port, ()=> {
  console.log(`Listening port: ${config.port}`);
});