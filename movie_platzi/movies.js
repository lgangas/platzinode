const moviesMock = [{
    "id": 1,
    "title": "Harem suare",
    "year": 2004,
    "cover": "http://dummyimage.com/123x138.png/ff4444/ffffff",
    "description": "Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.\n\nIn congue. Etiam justo. Etiam pretium iaculis justo.\n\nIn hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.",
    "duration": 88,
    "contentRating": "A",
    "source": "https://netscape.com/massa.xml?non=neque&mauris=duis&morbi=bibendum&non=morbi&lectus=non&aliquam=quam&sit=nec&amet=dui&diam=luctus&in=rutrum&magna=nulla&bibendum=tellus&imperdiet=in&nullam=sagittis&orci=dui&pede=vel&venenatis=nisl&non=duis&sodales=ac&sed=nibh&tincidunt=fusce&eu=lacus&felis=purus&fusce=aliquet&posuere=at&felis=feugiat&sed=non&lacus=pretium&morbi=quis&sem=lectus&mauris=suspendisse&laoreet=potenti&ut=in&rhoncus=eleifend&aliquet=quam&pulvinar=a&sed=odio&nisl=in&nunc=hac&rhoncus=habitasse&dui=platea&vel=dictumst&sem=maecenas&sed=ut&sagittis=massa&nam=quis&congue=augue&risus=luctus&semper=tincidunt&porta=nulla&volutpat=mollis&quam=molestie&pede=lorem&lobortis=quisque&ligula=ut&sit=erat&amet=curabitur&eleifend=gravida&pede=nisi&libero=at&quis=nibh&orci=in&nullam=hac&molestie=habitasse&nibh=platea&in=dictumst&lectus=aliquam&pellentesque=augue&at=quam&nulla=sollicitudin&suspendisse=vitae&potenti=consectetuer&cras=eget&in=rutrum&purus=at&eu=lorem&magna=integer&vulputate=tincidunt&luctus=ante&cum=vel&sociis=ipsum&natoque=praesent&penatibus=blandit&et=lacinia&magnis=erat&dis=vestibulum&parturient=sed&montes=magna&nascetur=at&ridiculus=nunc&mus=commodo&vivamus=placerat",
    "tags": ["Comedy", "Comedy|Drama", "Drama"]
  },
  {
    "id": 2,
    "title": "3 Blind Mice",
    "year": 2006,
    "cover": "http://dummyimage.com/111x179.png/cc0000/ffffff",
    "description": "Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.",
    "duration": 78,
    "contentRating": "B",
    "source": "https://51.la/suspendisse.png?vulputate=dui&ut=maecenas&ultrices=tristique&vel=est&augue=et&vestibulum=tempus&ante=semper&ipsum=est&primis=quam&in=pharetra&faucibus=magna&orci=ac&luctus=consequat&et=metus&ultrices=sapien&posuere=ut&cubilia=nunc&curae=vestibulum&donec=ante&pharetra=ipsum&magna=primis&vestibulum=in&aliquet=faucibus&ultrices=orci&erat=luctus&tortor=et&sollicitudin=ultrices&mi=posuere&sit=cubilia&amet=curae&lobortis=mauris&sapien=viverra&sapien=diam&non=vitae&mi=quam&integer=suspendisse&ac=potenti&neque=nullam&duis=porttitor&bibendum=lacus&morbi=at&non=turpis&quam=donec&nec=posuere&dui=metus&luctus=vitae&rutrum=ipsum&nulla=aliquam&tellus=non&in=mauris&sagittis=morbi&dui=non&vel=lectus&nisl=aliquam&duis=sit&ac=amet&nibh=diam&fusce=in&lacus=magna&purus=bibendum&aliquet=imperdiet&at=nullam&feugiat=orci&non=pede&pretium=venenatis&quis=non&lectus=sodales&suspendisse=sed&potenti=tincidunt&in=eu&eleifend=felis&quam=fusce&a=posuere&odio=felis&in=sed&hac=lacus&habitasse=morbi&platea=sem&dictumst=mauris&maecenas=laoreet&ut=ut&massa=rhoncus&quis=aliquet&augue=pulvinar&luctus=sed&tincidunt=nisl&nulla=nunc&mollis=rhoncus&molestie=dui&lorem=vel&quisque=sem&ut=sed&erat=sagittis&curabitur=nam&gravida=congue&nisi=risus&at=semper&nibh=porta",
    "tags": ["Drama", "Comedy"]
  },
  {
    "id": 3,
    "title": "Breakfast of Champions",
    "year": 2008,
    "cover": "http://dummyimage.com/230x238.jpg/dddddd/000000",
    "description": "Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.\n\nPellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.",
    "duration": 83,
    "contentRating": "x",
    "source": "http://ucla.edu/diam/id/ornare/imperdiet/sapien/urna.png?nunc=vulputate&proin=luctus&at=cum&turpis=sociis&a=natoque&pede=penatibus&posuere=et&nonummy=magnis&integer=dis&non=parturient&velit=montes&donec=nascetur&diam=ridiculus&neque=mus&vestibulum=vivamus&eget=vestibulum&vulputate=sagittis&ut=sapien&ultrices=cum&vel=sociis",
    "tags": ["Horror|Thriller"]
  },
  {
    "id": 4,
    "title": "Walk Among the Tombstones, A",
    "year": 1990,
    "cover": "http://dummyimage.com/196x138.png/dddddd/000000",
    "description": "Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.\n\nAenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.",
    "duration": 74,
    "contentRating": "A",
    "source": "https://va.gov/duis/consequat/dui/nec/nisi/volutpat/eleifend.jpg?non=parturient&mauris=montes&morbi=nascetur&non=ridiculus&lectus=mus&aliquam=etiam&sit=vel&amet=augue&diam=vestibulum&in=rutrum&magna=rutrum&bibendum=neque&imperdiet=aenean&nullam=auctor&orci=gravida&pede=sem&venenatis=praesent&non=id&sodales=massa&sed=id&tincidunt=nisl&eu=venenatis&felis=lacinia&fusce=aenean&posuere=sit&felis=amet&sed=justo&lacus=morbi&morbi=ut&sem=odio&mauris=cras&laoreet=mi&ut=pede&rhoncus=malesuada",
    "tags": ["Drama", "Comedy", "Comedy|Drama|Romance"]
  },
  {
    "id": 5,
    "title": "Lost Son, The",
    "year": 2006,
    "cover": "http://dummyimage.com/105x230.bmp/cc0000/ffffff",
    "description": "Fusce consequat. Nulla nisl. Nunc nisl.\n\nDuis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.\n\nIn hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.",
    "duration": 51,
    "contentRating": "x",
    "source": "http://umich.edu/eget/congue.png?nam=placerat&ultrices=praesent&libero=blandit&non=nam",
    "tags": ["Drama"]
  },
  {
    "id": 6,
    "title": "Shivers (They Came from Within)",
    "year": 2008,
    "cover": "http://dummyimage.com/212x241.jpg/ff4444/ffffff",
    "description": "Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.",
    "duration": 64,
    "contentRating": "B",
    "source": "http://freewebs.com/odio.js?id=id&ornare=pretium&imperdiet=iaculis&sapien=diam&urna=erat&pretium=fermentum&nisl=justo&ut=nec&volutpat=condimentum&sapien=neque&arcu=sapien&sed=placerat&augue=ante&aliquam=nulla&erat=justo&volutpat=aliquam&in=quis&congue=turpis&etiam=eget&justo=elit&etiam=sodales&pretium=scelerisque&iaculis=mauris&justo=sit&in=amet&hac=eros&habitasse=suspendisse&platea=accumsan&dictumst=tortor&etiam=quis&faucibus=turpis&cursus=sed&urna=ante&ut=vivamus&tellus=tortor&nulla=duis&ut=mattis&erat=egestas&id=metus&mauris=aenean&vulputate=fermentum&elementum=donec&nullam=ut&varius=mauris&nulla=eget&facilisi=massa&cras=tempor&non=convallis&velit=nulla&nec=neque&nisi=libero&vulputate=convallis&nonummy=eget&maecenas=eleifend&tincidunt=luctus&lacus=ultricies&at=eu&velit=nibh&vivamus=quisque&vel=id&nulla=justo&eget=sit&eros=amet&elementum=sapien&pellentesque=dignissim&quisque=vestibulum&porta=vestibulum&volutpat=ante&erat=ipsum&quisque=primis&erat=in&eros=faucibus&viverra=orci&eget=luctus&congue=et&eget=ultrices",
    "tags": ["Documentary", "Comedy|Drama|Romance", "Crime|Thriller", "Fantasy|Horror"]
  },
  {
    "id": 7,
    "title": "Cooking with Stella",
    "year": 1995,
    "cover": "http://dummyimage.com/142x202.jpg/5fa2dd/ffffff",
    "description": "Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\n\nEtiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.\n\nPraesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.",
    "duration": 35,
    "contentRating": "B",
    "source": "https://desdev.cn/vestibulum/ante/ipsum/primis/in/faucibus.png?elementum=odio&pellentesque=curabitur&quisque=convallis&porta=duis&volutpat=consequat&erat=dui&quisque=nec&erat=nisi&eros=volutpat&viverra=eleifend&eget=donec&congue=ut&eget=dolor&semper=morbi&rutrum=vel&nulla=lectus&nunc=in&purus=quam&phasellus=fringilla&in=rhoncus&felis=mauris&donec=enim&semper=leo&sapien=rhoncus&a=sed&libero=vestibulum&nam=sit&dui=amet&proin=cursus&leo=id&odio=turpis&porttitor=integer&id=aliquet&consequat=massa&in=id&consequat=lobortis&ut=convallis&nulla=tortor&sed=risus&accumsan=dapibus&felis=augue&ut=vel&at=accumsan&dolor=tellus&quis=nisi&odio=eu&consequat=orci&varius=mauris&integer=lacinia&ac=sapien&leo=quis&pellentesque=libero&ultrices=nullam&mattis=sit&odio=amet&donec=turpis&vitae=elementum&nisi=ligula&nam=vehicula&ultrices=consequat&libero=morbi&non=a&mattis=ipsum&pulvinar=integer&nulla=a&pede=nibh&ullamcorper=in&augue=quis&a=justo&suscipit=maecenas&nulla=rhoncus&elit=aliquam&ac=lacus&nulla=morbi&sed=quis&vel=tortor&enim=id&sit=nulla&amet=ultrices&nunc=aliquet&viverra=maecenas&dapibus=leo&nulla=odio&suscipit=condimentum&ligula=id&in=luctus&lacus=nec&curabitur=molestie&at=sed",
    "tags": ["(no genres listed)", "Comedy", "Drama|Romance", "Drama", "Comedy"]
  },
  {
    "id": 8,
    "title": "Second in Command",
    "year": 2011,
    "cover": "http://dummyimage.com/169x191.png/dddddd/000000",
    "description": "Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.\n\nNam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.\n\nCurabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.",
    "duration": 97,
    "contentRating": "A",
    "source": "https://tuttocitta.it/lobortis/vel/dapibus/at/diam.json?iaculis=ac&congue=neque&vivamus=duis&metus=bibendum&arcu=morbi&adipiscing=non&molestie=quam&hendrerit=nec&at=dui&vulputate=luctus&vitae=rutrum&nisl=nulla&aenean=tellus&lectus=in&pellentesque=sagittis&eget=dui&nunc=vel&donec=nisl&quis=duis&orci=ac&eget=nibh&orci=fusce&vehicula=lacus&condimentum=purus&curabitur=aliquet&in=at&libero=feugiat&ut=non&massa=pretium&volutpat=quis&convallis=lectus&morbi=suspendisse&odio=potenti&odio=in&elementum=eleifend&eu=quam&interdum=a&eu=odio&tincidunt=in&in=hac&leo=habitasse&maecenas=platea&pulvinar=dictumst&lobortis=maecenas&est=ut&phasellus=massa&sit=quis&amet=augue&erat=luctus&nulla=tincidunt&tempus=nulla&vivamus=mollis&in=molestie&felis=lorem&eu=quisque&sapien=ut&cursus=erat&vestibulum=curabitur&proin=gravida&eu=nisi&mi=at&nulla=nibh&ac=in&enim=hac&in=habitasse&tempor=platea&turpis=dictumst&nec=aliquam&euismod=augue&scelerisque=quam&quam=sollicitudin&turpis=vitae&adipiscing=consectetuer&lorem=eget&vitae=rutrum&mattis=at&nibh=lorem&ligula=integer&nec=tincidunt&sem=ante",
    "tags": ["Children|Comedy", "Comedy", "Drama", "Comedy"]
  },
  {
    "id": 9,
    "title": "Devil Came on Horseback, The",
    "year": 2000,
    "cover": "http://dummyimage.com/208x180.jpg/dddddd/000000",
    "description": "Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.\n\nAenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.\n\nCurabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.",
    "duration": 89,
    "contentRating": "x",
    "source": "https://dropbox.com/hac/habitasse.json?augue=praesent&aliquam=id",
    "tags": ["Action|Drama|Western", "Action|Drama"]
  },
  {
    "id": 10,
    "title": "Week-End at the Waldorf",
    "year": 2011,
    "cover": "http://dummyimage.com/234x244.bmp/dddddd/000000",
    "description": "Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.\n\nCras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.",
    "duration": 51,
    "contentRating": "B",
    "source": "http://europa.eu/molestie/sed/justo.aspx?ridiculus=lectus&mus=pellentesque&etiam=eget&vel=nunc&augue=donec&vestibulum=quis&rutrum=orci&rutrum=eget&neque=orci&aenean=vehicula&auctor=condimentum&gravida=curabitur&sem=in&praesent=libero&id=ut&massa=massa&id=volutpat&nisl=convallis&venenatis=morbi&lacinia=odio&aenean=odio&sit=elementum&amet=eu&justo=interdum&morbi=eu&ut=tincidunt&odio=in&cras=leo&mi=maecenas&pede=pulvinar&malesuada=lobortis&in=est&imperdiet=phasellus&et=sit&commodo=amet&vulputate=erat&justo=nulla&in=tempus&blandit=vivamus&ultrices=in&enim=felis&lorem=eu&ipsum=sapien&dolor=cursus&sit=vestibulum&amet=proin&consectetuer=eu&adipiscing=mi&elit=nulla&proin=ac&interdum=enim&mauris=in&non=tempor&ligula=turpis&pellentesque=nec&ultrices=euismod&phasellus=scelerisque&id=quam&sapien=turpis&in=adipiscing&sapien=lorem&iaculis=vitae&congue=mattis&vivamus=nibh&metus=ligula&arcu=nec&adipiscing=sem&molestie=duis&hendrerit=aliquam&at=convallis&vulputate=nunc&vitae=proin",
    "tags": ["Adventure|Comedy|Romance", "Adventure|Comedy|Sci-Fi", "Crime|Horror|Sci-Fi"]
  }
];

module.exports = {
  moviesMock
};