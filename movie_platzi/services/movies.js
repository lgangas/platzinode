const  { moviesMock } = require('../movies');

class MovieServices {

  async getMovies(tags = []) {
    const movies = await Promise.resolve(moviesMock);
    return movies || [];
  }

  async getMovie(id) {
    const movie = await Promise.resolve(moviesMock.find(m => m.id == id));
    return movie || {};
  }

  async createMovie(movie) {
    const id = await Promise.resolve(1000);
    return id;
  }

  async updateMovie(idMovie, movie) {
    const id = await Promise.resolve(1000);
    return id;
  }

  async deleteMovie(idMovie) {
    const id = await Promise.resolve(1000);
    return id;
  }

}

module.exports = MovieServices;