const express = require('express');
const {
  moviesMock
} = require('../movies.js')
const MovieService = require('../services/movies');

function moviesAPI(app) {

  const router = express.Router();
  app.use('/api/movies', router);

  const movieService = new MovieService();

  router.get('/', async function (req, res, next) {
    const { tags } = req.query;
    try {
      const movies = await movieService.getMovies(tags);

      res.status(200).json({
        data: movies,
        message: 'Movies listed',
      });
    } catch (err) {
      next(err);
    }
  });

  router.get('/:id', async function (req, res, next) {
    const { id } = req.params;
    try {
      const movie = await movieService.getMovie(id);

      res.status(200).json({
        data: movie,
        message: 'Movie listed',
      });
    } catch (err) {
      next(err);
    }
  });

  router.post('/', async function (req, res, next) {
    const { body } = req;
    try {
      const id = await movieService.createMovie(body);

      res.status(200).json({
        data: id,
        message: 'Movie created',
      });
    } catch (err) {
      next(err);
    }
  });

  router.patch('/:id', async function (req, res, next) {
    const { id } = req.params;
    const { body } = req;
    try {
      const id = await movieService.updateMovie(id, body);

      res.status(200).json({
        data: id,
        message: 'Movie updated',
      });
    } catch (err) {
      next(err);
    }
  });

  router.delete('/:id', async function (req, res, next) {
    const { id } = req.params;
    try {
      const id = await movieService.deleteMovie(id);

      res.status(200).json({
        data: id,
        message: 'Movie deleted',
      });
    } catch (err) {
      next(err);
    }
  });
}

module.exports = moviesAPI;